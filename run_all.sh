# # BPR模型
# python run_recbole.py --model=BPR --dataset=book-crossing
# python run_recbole.py --model=BPR --dataset=douban-movie
# python run_recbole.py --model=BPR --dataset=epinious
# python run_recbole.py --model=BPR --dataset=ml-1m
# python run_recbole.py --model=BPR --dataset=ml-100k
# python run_recbole.py --model=BPR --dataset=book-crossing
# echo "============BPR finished======"
# # DMF
# python run_recbole.py --model=DMF --dataset=book-crossing
# python run_recbole.py --model=DMF --dataset=douban-movie
# python run_recbole.py --model=DMF --dataset=epinious
# python run_recbole.py --model=DMF --dataset=ml-1m
# python run_recbole.py --model=DMF --dataset=ml-100k
# python run_recbole.py --model=DMF --dataset=book-crossing
# echo "============DMF finished======"
# # NeuMF
# python run_recbole.py --model=NeuMF --dataset=book-crossing
# python run_recbole.py --model=NeuMF --dataset=douban-movie
# python run_recbole.py --model=NeuMF --dataset=epinious
# python run_recbole.py --model=NeuMF --dataset=ml-1m
# python run_recbole.py --model=NeuMF --dataset=ml-100k
# python run_recbole.py --model=NeuMF --dataset=book-crossing
# echo "============NeuMF finished======"
# # GCMC
# python run_recbole.py --model=GCMC --dataset=book-crossing
# python run_recbole.py --model=GCMC --dataset=douban-movie
# python run_recbole.py --model=GCMC --dataset=epinious
# python run_recbole.py --model=GCMC --dataset=ml-1m
# python run_recbole.py --model=GCMC --dataset=ml-100k
# python run_recbole.py --model=GCMC --dataset=book-crossing
# echo "============GCMC finished======"
# NGCF
python run_recbole.py --model=NGCF --dataset=book-crossing
python run_recbole.py --model=NGCF --dataset=douban-movie
python run_recbole.py --model=NGCF --dataset=epinious
python run_recbole.py --model=NGCF --dataset=ml-1m
python run_recbole.py --model=NGCF --dataset=ml-100k
python run_recbole.py --model=NGCF --dataset=book-crossing
echo "============NGCF finished======"
# LightGCN
python run_recbole.py --model=LightGCN --dataset=book-crossing
python run_recbole.py --model=LightGCN --dataset=douban-movie
python run_recbole.py --model=LightGCN --dataset=epinious
python run_recbole.py --model=LightGCN --dataset=ml-1m
python run_recbole.py --model=LightGCN --dataset=ml-100k
python run_recbole.py --model=LightGCN --dataset=book-crossing
echo "============LightGCN finished======"
# DGCF
python run_recbole.py --model=DGCF --dataset=book-crossing
python run_recbole.py --model=DGCF --dataset=douban-movie
python run_recbole.py --model=DGCF --dataset=epinious
python run_recbole.py --model=DGCF --dataset=ml-1m
python run_recbole.py --model=DGCF --dataset=ml-100k
python run_recbole.py --model=DGCF --dataset=book-crossing
echo "============DGCF finished======"